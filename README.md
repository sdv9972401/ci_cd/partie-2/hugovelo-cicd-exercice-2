# 🚴 HugoVelo - Site web statique avec Hugo et AutoDevOps

[![pipeline status](https://gitlab.com/sdv9972401/ci_cd/partie-2/hugovelo-cicd-exercice-1/badges/main/pipeline.svg)](https://gitlab.com/sdv9972401/ci_cd/partie-2/hugovelo-cicd-exercice-1/-/commits/main)

![](screenshot.png)


L'ensemble de la documentation de l'exercice est disponible sur le [wiki du projet](https://gitlab.com/sdv9972401/ci_cd/partie-2/hugovelo-cicd-exercice-2/-/wikis/home).
